# Eloquent Migrations

This package for the use "Eloquent" migration system without Laravel framevork

After downloading the package to the vendor folder, follow these steps:

1) type the command in the console:

'./vendor/bin/rob init'

Use this command to create the necessary directories

'/database'

and configuration file:

'migration_config.php' //with DB settings and file location paths

After editing the DB configuration settings, you will see the following list of commands...

List of available commands:

'./vendor/bin/rob init' # Init the project
'./vendor/bin/rob create' MyMigration [--table=users] [--create=users] # Create a new migration
'./vendor/bin/rob migrate' # Run all available migrations
'./vendor/bin/rob rollback' # Rollback the last migration. The option --step 3 allows you to rollback multiple migrations
'./vendor/bin/rob status' # Display the migrations status
'./vendor/bin/rob seed:create' MySeed # Create a new seed
'./vendor/bin/rob seed:run' # Run all seeds
