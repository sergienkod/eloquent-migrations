<?php

$baseConfigMigrator = [
    //DB connect config
    'DBDRIVER'          => 'mysql',
    'DBHOST'            => 'localhost',
    'DBNAME'            => '',
    'DBUSER'            => 'root',
    'DBPASS'            => '',
    'PREFIX_DB_TABLE'   => '',
    'DB_COLLATION'      => 'utf8_unicode_ci',
    // path config
    'CONFIG_FILE' => 'elmigrator.php',        // name of config file in the repository of project
    'PATH_TO_REPOSITORY'=> ROOT_DIRR.'/database',       //absolute path to the repository of the project
    'PATH_TO_MIGRATIONS'=> ROOT_DIRR.'/database/migrations',        //absolute path to the directory of the migrations
    'PATH_TO_SEEDS'     => ROOT_DIRR.'/database/seeds'      //absolute path to the directory of the seeds
];