<?php

require_once __DIR__ . '/../../../autoload.php';


use Symfony\Component\Console\Application;
use Hyde1\EloquentMigrations\Command;

defined('ROOT_DIRR') or   define('ROOT_DIRR',__DIR__.'/../../../..');//absolute path to the root of the project

$app = new Application();

$app->addCommands([
    new Command\Init(),
    new Command\CreateMigration(),
    new Command\Migrate(),
    new Command\FreshMigration(),
    new Command\Rollback(),
    new Command\Status(),
    new Command\CreateSeed(),
    new Command\RunSeed(),
]);
$app->run();
return $app;
