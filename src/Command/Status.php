<?php

namespace Hyde1\EloquentMigrations\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Hyde1\EloquentMigrations\Migrations\Migrator;
use Illuminate\Database\Migrations\DatabaseMigrationRepository;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Illuminate\Support\Composer;

class Status extends AbstractCommand
{
	protected static $defaultName = 'status';

	/**
     * The migration creator instance.
     *
     * @var \Illuminate\Database\Migrations\Migrator
     */
    protected $migrator;

	/**
	 * The migration repository
	 *
	 * @var DatabaseMigrationRepository
	 */
	protected $repository;

	protected function configure()
	{
		$this
			->setDescription('Display migration status')
			->setHelp('Show the status of each migration'.PHP_EOL);

		parent::configure();
	}

	public function execute(InputInterface $input, OutputInterface $output)
	{
		$this->bootstrap($input, $output);
		$this->repository = new DatabaseMigrationRepository($this->getDb(), $this->getMigrationTable());
		$this->migrator = new Migrator($this->repository, $this->getDb(), new Filesystem);

		if (! $this->migrator->repositoryExists()) {
			throw new \RuntimeException('The migration table is not installed');
		}

		$ran = $this->migrator->getRepository()->getRan();
		$batches = $this->migrator->getRepository()->getRan();


        if (count($migrations = $this->getStatusFor($ran)) > 0) {
            $this->table(['Ran?', 'Migration'], $migrations->toArray());
        } else {
            $output->writeln('<error>No migrations found</error>');
        }

	}

    protected function getStatusFor(array $ran)
    {
        return \Illuminate\Support\Collection::make($this->getAllMigrationFiles())
            ->map(function ($migration) use ($ran) {
                $migrationName = $this->migrator->getMigrationName($migration);

                return in_array($migrationName, $ran)
                    ? ['<info>Y</info>', $migrationName]
                    : ['<fg=red>N</fg=red>', $migrationName];
            });
    }

	/**
	 * Get an array of all of the migration files.
	 *
	 * @return array
	 */
	protected function getAllMigrationFiles()
	{
		return $this->migrator->getMigrationFiles([$this->getMigrationPath()]);
	}
}
