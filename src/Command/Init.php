<?php

namespace Hyde1\EloquentMigrations\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class Init extends AbstractCommand
{
	protected static $defaultName = 'init';

	protected function configure()
	{
		$this->setDescription('Initialize the project')
			 ->setHelp('Initialize the project for Eloquent Migrations'.PHP_EOL);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
        $this->beforLoadConfig($output);// bootstrap($input, $output);
		$path = $this->createPath($this->baseConfig['PATH_TO_REPOSITORY'], $output);
		$this->createConfig($path, $output);
        $this->createPath($this->baseConfig['PATH_TO_MIGRATIONS'], $output);
        $this->createPath($this->baseConfig['PATH_TO_SEEDS'], $output);
	}

	protected function createPath($path, OutputInterface $output):string
	{

		if (is_dir($path)) {
			return $path;
		}

		if (!is_dir($path) && !mkdir($path)) {
			throw new InvalidArgumentException(sprintf(
				'Cannot create `%s` directory',
				$path
			));
		}

		if (!is_writable($path) || is_file($path)) {
			throw new InvalidArgumentException(sprintf(
				'The directory `%s` is not writable',
				$path
			));
		}

		$output->writeln("<info>created</info> $path");
		return $path;
	}

	protected function createConfig(string $path, OutputInterface $output)
	{
		$configfile = 'elmigrator.php';
		$contents = file_get_Contents(__DIR__ . '/../../data/'. $configfile . '.dist');

		if ($contents === false) {
			throw new RuntimeException('Could not find template for config file');
		}

		$outputPath = $path . DIRECTORY_SEPARATOR . $configfile;
		$ret = file_put_contents($outputPath, $contents);
		if ($ret === false) {
			throw new RuntimeException(sprintf(
				'The config file `%s` could not be written',
				$configfile
			));
		}

		$output->writeln("<info>created</info> $outputPath");
	}
}
